package net.biomodels.jummp.indexing;

import org.sbml.jsbml.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.tree.TreeNode;
import javax.xml.stream.XMLStreamException;
import java.io.*;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Created by Tung on 05/01/2018.
 */
public class SBMLHandler {
    private static final Logger log = LoggerFactory.getLogger(SBMLHandler.class);
    private static long now = System.nanoTime();
    public static void main(String[] args) {
        String sbmlFilePath = "src/main/resources/models/BIOMD0000000002_url.xml";
        /**
         * download the biggest model MODEL1311110001.xml from the link
         * http://wwwdev.ebi.ac.uk/biomodels/MODEL1311110001
         * and test it by updating the above variable
         */

        try {
            Model m = parseSbmlModel(sbmlFilePath);
            System.out.println(m);
            Integer count1 = countTreeNode(m);
            Long count2 = convertModelToStream(m).count();
            System.out.println(count1);
            System.out.println(count2);
            // Get the Java runtime
            Runtime runtime = Runtime.getRuntime();
            // Run the garbage collector
            runtime.gc();
            // Calculate the used memory
            long memory = runtime.totalMemory() - runtime.freeMemory();
            System.out.println("Used memory is bytes: " + memory);
            System.out.println("Used memory is megabytes: "
                    + PerformanceTest.bytesToMegabytes(memory));
        } catch (XMLStreamException | FileNotFoundException ignored) {
            throw new IllegalStateException("The model is not a valid XML file.");
        }
    }

    public static int countTreeNode(TreeNode treeNode) {
        Enumeration<TreeNode> children = treeNode.children();
        int total = 0;
        if (treeNode instanceof SBase && !(treeNode instanceof ListOf)) {
            total += 1;
        }
        while (children.hasMoreElements()) {
            TreeNode next = children.nextElement();
            total += countTreeNode(next);
        }
        return total;
    }

    public static Model parseSbmlModel(String path) throws XMLStreamException, FileNotFoundException {
        SBMLDocument d = null;
        Model model;
        try (InputStream in = new BufferedInputStream(new FileInputStream(path))) {
            SBMLReader r = new SBMLReader();
            d = r.readSBMLFromStream(in);
            long parseTime = System.nanoTime() - now;
            log.info("JSBML parseTime: {}", PerformanceTest.formatDuration(parseTime));
            now = System.nanoTime(); // reset timer for processing time
        } catch (IOException e) {
            log.error("I/O exception while reading " + path, e);
        }
        if (null == d) {
            return null;
        }
        model = d.getModel();
        return model;
    }

    public static Stream<? extends SBase> convertModelToStream(Model model) {
        return visit(model);
    }

    private static <S extends SBase> Stream<S> visit(TreeNode n) {
        Objects.requireNonNull(n, "Refusing to process an undefined element");
        boolean isSBase = n instanceof SBase;
        if (!isSBase) {
            return Stream.empty();
        }

        Stream<S> childStream = StreamSupport
                .stream(new CompositeSBaseSpliterator(n), false)
                .flatMap(SBMLHandler::<S>visit);
        if (n instanceof ListOf) {
            return childStream;
        }
        //noinspection unchecked -- we have already checked n is a kind of SBase
        return Stream.concat(
                Stream.of((S) n),
                childStream
        );
    }
}

/* fixed-size spliterators are used to partition streams into batches */
// https://www.airpair.com/java/posts/parallel-processing-of-io-based-data-with-java-streams
@SuppressWarnings("WeakerAccess")
abstract class FixedBatchSpliteratorBase<T> implements Spliterator<T> {
    private final int batchSize;
    private final int characteristics;
    private long est;

    public FixedBatchSpliteratorBase(int characteristics, int batchSize, long est) {
        this.characteristics = characteristics | SUBSIZED;
        this.batchSize = batchSize;
        this.est = est;
    }

    public FixedBatchSpliteratorBase(int characteristics, int batchSize) {
        this(characteristics, batchSize, Long.MAX_VALUE);
    }

    public FixedBatchSpliteratorBase(int characteristics) {
        this(characteristics, 128, Long.MAX_VALUE);
    }

    @SuppressWarnings("unused")
    public FixedBatchSpliteratorBase() {
        this(IMMUTABLE | ORDERED | NONNULL);
    }

    @Override
    public Spliterator<T> trySplit() {
        final HoldingConsumer<T> holder = new HoldingConsumer<>();
        if (!tryAdvance(holder)) return null;
        final Object[] a = new Object[batchSize];
        int j = 0;
        do a[j] = holder.value; while (++j < batchSize && tryAdvance(holder));
        if (est != Long.MAX_VALUE) est -= j;
        return Spliterators.spliterator(a, 0, j, characteristics() | SIZED);
    }

    @Override
    public Comparator<? super T> getComparator() {
        if (hasCharacteristics(SORTED)) return null;
        throw new IllegalStateException();
    }

    @Override
    public long estimateSize() {
        return est;
    }

    @Override
    public int characteristics() {
        return characteristics;
    }

    static final class HoldingConsumer<T> implements Consumer<T> {
        Object value;

        @Override
        public void accept(T value) {
            this.value = value;
        }
    }
}

/* we rely on JSBML's TreeNode support to recursively a stream of SBase entities */
final class CompositeSBaseSpliterator extends FixedBatchSpliteratorBase<SBase> {
    private static final int FLAGS = Spliterator.ORDERED | Spliterator.IMMUTABLE | Spliterator.NONNULL;
    private static final int DEFAULT_BATCH_SIZE = 10;
    private final Enumeration<TreeNode> collection;

    @SuppressWarnings("unused")
    CompositeSBaseSpliterator(TreeNode parent) {
        this(parent, DEFAULT_BATCH_SIZE);
    }

    @SuppressWarnings({"unchecked", "WeakerAccess"})
    CompositeSBaseSpliterator(TreeNode parent, int batchSize) {
        super(FLAGS, batchSize);
        this.collection = parent.children();
    }

    @Override
    public boolean tryAdvance(Consumer<? super SBase> action) {
        if (!collection.hasMoreElements()) return false;

        TreeNode next = collection.nextElement();
        if (!(next instanceof SBase)) {
            return tryAdvance(action);
        }
        action.accept((SBase) next);
        return true;
    }
}