package net.biomodels.jummp.indexing;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class DatabaseManager {
    String userName;
    String password;
    String serverName;
    String dbName;
    Integer portNumber;
    String dbms;

    public DatabaseManager(String userName, String password, String serverName,
                           String dbName, Integer portNumber, String dbms) {
        this.userName = userName;
        this.password = password;
        this.serverName = serverName;
        this.dbName = dbName;
        this.portNumber = portNumber;
        this.dbms = dbms;
    }

    public Connection getConnection() throws SQLException {
        Connection conn = null;
        Properties connectionProps = new Properties();
        connectionProps.put("user", this.userName);
        connectionProps.put("password", this.password);

        if (this.dbms.equals("mysql")) {
            conn = DriverManager.getConnection(
                    "jdbc:" + this.dbms + "://" +
                            this.serverName +
                            ":" + this.portNumber + "/" + this.dbName,
                    connectionProps);
        } else if (this.dbms.equals("derby")) {
            conn = DriverManager.getConnection(
                    "jdbc:" + this.dbms + ":" +
                            this.dbName +
                            ";create=true",
                    connectionProps);
        }
        System.out.println("Connected to the database " + conn.getCatalog());
        return conn;
    }

    public Long getLatestRevision(Connection connection, Long modelId) throws SQLException {
        Statement stmt = null;
        Long latestRevisionNumber = 0L;
        String query = "select * from revision where revision_number = (select max(revision_number) from revision " +
                "where model_id = " + modelId + ") and model_id = " + modelId;
        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            if (rs.next()) {
                latestRevisionNumber = rs.getLong("id");
            }
        } catch (SQLException e) {
            System.out.println("Error: " + e.toString());
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }

        return latestRevisionNumber;
    }

    public Long getNumberOfAnnotations(Connection connection, Long revisionId) throws SQLException {
        Statement stmt = null;
        Long nbAnnotations = 0L;
        String query = "select count(*) as nbAnnotations from revision_annotations where revision_id = " + revisionId;
        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            if (rs.next()) {
                nbAnnotations = rs.getLong("nbAnnotations");
            }
        } catch (SQLException e) {
            System.out.println("Error: " + e.toString());
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
        return nbAnnotations;
    }

    public Map<String, String> getModelInfo(Connection connection, Long modelId) throws SQLException {
        Map<String, String> modelInfoMap = new HashMap<>();
        Statement stmt = null;
        String query = "select model.submission_id, model.vcs_identifier, revision.id as latest_revision, " +
                "repository_file.path, model_format.name as format_name  " +
                "from model join revision on revision.model_id = model.id join repository_file on " +
                "repository_file.revision_id = revision.id join model_format on revision.format_id = model_format.id " +
                "where model.id = " + modelId + " and repository_file.main_file = 1";
        try {
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            if (rs.next()) {
                String submissionId = rs.getString("submission_id");
                String vcsIdentifier = rs.getString("vcs_identifier");
                Long latestRevisionId = rs.getLong("latest_revision");
                String mainFilePath = rs.getString("path");
                String formatName = rs.getString("format_name");
                modelInfoMap.put("submission_id", submissionId);
                modelInfoMap.put("vcs_identifier", vcsIdentifier);
                modelInfoMap.put("latest_revision", latestRevisionId.toString());
                modelInfoMap.put("main_file_path", mainFilePath);
                modelInfoMap.put("format_name", formatName);
            }
        } catch (SQLException e) {
            System.out.println("Error: " + e.toString());
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
        return modelInfoMap;
    }
}
