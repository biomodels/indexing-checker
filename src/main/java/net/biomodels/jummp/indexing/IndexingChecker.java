package net.biomodels.jummp.indexing;

import org.apache.commons.cli.*;
import org.sbml.jsbml.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLStreamException;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Stream;

public class IndexingChecker {
    private static final Logger log = LoggerFactory.getLogger(IndexingChecker.class);
    private static long now = System.nanoTime();
    private static DatabaseManager dbManager;
    private static Properties jummpProps;
    private static BufferedWriter output;

    public void init() {
        System.out.println("Initialising...");
        String jummpPropertiesFilePath = System.getenv("JUMMP_CONFIG");
        if (jummpPropertiesFilePath == null) {
            String folder = System.getProperty("user.home") + System.getProperty("file.separator");
            jummpPropertiesFilePath =  folder + ".jummp.properties";
            if (!(new File(jummpPropertiesFilePath).exists())) {
                log.debug("Cannot find the default configuration file for the global settings");
                System.out.println("Cannot find the default configuration file for the global settings");
                return;
            }
        } else {
            File config = new File(jummpPropertiesFilePath);
            if (!config.exists()) {
                log.debug("Cannot find the customised configuration file for the global settings");
                System.out.println("Cannot find the customised configuration file for the global settings");
                return;
            }
        }
        jummpProps = new Properties();
        Reader reader;
        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(jummpPropertiesFilePath), "UTF-8"));
            jummpProps.load(reader);
            String userName = jummpProps.getProperty("jummp.database.username");
            String password = jummpProps.getProperty("jummp.database.password");
            String serverName = jummpProps.getProperty("jummp.database.server");
            String dbName = jummpProps.getProperty("jummp.database.database");
            Integer portNumber = Integer.parseInt(jummpProps.getProperty("jummp.database.port"));
            String dbType = jummpProps.getProperty("jummp.database.type");
            String dbms = dbType.toLowerCase();
            dbManager = new DatabaseManager(userName, password, serverName, dbName, portNumber, dbms);
            reader.close();
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public static void main(String[] args)  throws Exception {
        /* Ref: https://stackoverflow.com/questions/367706/how-to-parse-command-line-arguments-in-java */
        Options options = new Options();

        Option inputOpt = new Option("i", "input", true, "input file path " +
                "containing model identifiers which are checked the indices");
        inputOpt.setRequired(true);
        options.addOption(inputOpt);

        Option outputOpt = new Option("o", "output", true, "output file");
        outputOpt.setRequired(true);
        options.addOption(outputOpt);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("indexing-checker", options);
            System.exit(1);
            return;
        }

        String inputFilePath = cmd.getOptionValue("input");
        String outputFilePath = cmd.getOptionValue("output");

        System.out.println(inputFilePath);
        System.out.println(outputFilePath);

        IndexingChecker mainClass = new IndexingChecker();
        mainClass.init();
        System.out.println("Started...");
        Charset charset = Charset.forName("UTF-8");
        //Charset cs = StandardCharsets.UTF_8;
        OpenOption openOption = StandardOpenOption.APPEND;

        Path path = Paths.get(outputFilePath);
        try {
            output = Files.newBufferedWriter(path, charset, openOption);
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<String> lines;
        List<Long> models = new ArrayList<>();
        try {
            Path inputPath = Paths.get(inputFilePath);
            lines = Files.readAllLines(inputPath, charset);
            for (String line : lines) {
                models.add(Long.parseLong(line.trim()));
            }
        } catch (IOException e) {
            System.out.println(e);
        }

        try {
            Connection connection = dbManager.getConnection();
            Stream<Long> modelIds = models.parallelStream();
            modelIds.forEach((modelId) -> {
                try {
                    Long revisionId = dbManager.getLatestRevision(connection, modelId);
                    System.out.println("Revision: " + revisionId.toString());
                    Map<String, String> modelInfoMap = dbManager.getModelInfo(connection, modelId);
                    String workingDirectory = jummpProps.getProperty("jummp.vcs.workingDirectory");
                    String submissionId = modelInfoMap.get("submission_id");
                    String vcsIdentifier = modelInfoMap.get("vcs_identifier");
                    String mainFilePath = modelInfoMap.get("main_file_path");
                    String formatName = modelInfoMap.get("format_name");
                    String modelFilePath = workingDirectory + "/" + vcsIdentifier + mainFilePath;
                    if (formatName.equalsIgnoreCase("SBML")) {
                        Long nbAnnotations = dbManager.getNumberOfAnnotations(connection, revisionId);
                        System.out.println("Nb. Anno persisted inside database: " + nbAnnotations);
                        Model m = SBMLHandler.parseSbmlModel(modelFilePath);
                        // recursive traversal on TreeNode
                        Integer size = SBMLHandler.countTreeNode(m);
                        // use Stream in Java 8
                        //Long size = SBMLHandler.convertModelToStream(m).count();
                        System.out.println("Nb. Anno associated with SBML file: " + size);
                        log.info(submissionId, nbAnnotations, size);
                        output.write(submissionId + ", " + nbAnnotations + ", " + size + "\n");
                        output.flush();
                    }
                } catch (XMLStreamException | FileNotFoundException ignored) {
                    throw new IllegalStateException("The model is not a valid XML file.");
                } catch (Exception e) {
                    log.error("OOPS: ", e);
                }
            });
            output.close();
            connection.close();
        } catch (IOException | SQLException e) {
            System.out.println(e);
        } finally {
            now = System.nanoTime() - now;
            String duration = PerformanceTest.formatDuration(now);
            System.out.println("Finished within " + duration);
        }
    }
}